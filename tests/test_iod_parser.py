import unittest

from tdmconvert.iod_parser import IODParseError, parse_iod


class TestIOParser(unittest.TestCase):
    def test_fail_bad_iod_str(self):
        with self.assertRaises(IODParseError):
            parse_iod("40978 15 058Q   4171 E 2INvalidIODString")

    def test_valid_iod_str(self):
        message = parse_iod("40978 15 058Q   4171 E 20220508204215567 17 25 0414004+855477 37 R")
        self.assertEqual(message['norad_id'], 40978)

    def test_valid_iod_str_truncated_space(self):
        message = parse_iod("40978 15 058Q 4171 E 20220508204221796 17 25 0139362+861808 37 R")
        self.assertEqual(message['norad_id'], 40978)

    def test_valid_iod_str_missing_Col66(self):
        message = parse_iod("13553 82 092B   4171 E 20220508202236299 17 25 0802430+855090 37")
        self.assertEqual(message['norad_id'], 13553)
