"""Basic tests for the conversion from IOD strings to CCSDS TDM files.
"""

from pathlib import Path

from tdmconvert import iod2tdm, iod2tdm_file
from utils import assert_string

folder = Path(__file__).parent / "data"

IGNORE_STR = ['CREATION_DATE']


def test_iod2tdm_file(tmp_path):
    tmp_filename = tmp_path.joinpath('ccsds_tdm_5304653.xml')
    output_filename = folder.joinpath('ccsds_tdm_2020-09-15_seesat_PP1.xml')

    input_filename = folder.joinpath("2020-09-15_seesat_PP-Obs_14240_iod.txt")
    iod_str = Path(input_filename).read_text().strip()

    iod2tdm_file(iod_str, output_filename=tmp_filename, tdm_format='xml', originator='LSF')

    assert_string(tmp_filename.read_text(), output_filename.read_text(), ignore=IGNORE_STR)


def test_iod2tdm_one_path():
    # Read test input data
    input_filename = folder.joinpath("2020-09-15_seesat_PP-Obs_14240_iod.txt")
    iod_str = Path(input_filename).read_text().strip()

    # Run the method
    tdms = iod2tdm(iod_str, tdm_format='xml', originator='LSF')

    # Check the result
    assert (len(tdms) == 1)
    assert_string(tdms[0],
                  folder.joinpath('ccsds_tdm_2020-09-15_seesat_PP1.xml').read_text(),
                  ignore=IGNORE_STR)


def test_iod2tdm_three_paths():
    # Read test input data
    input_filename = folder.joinpath("2020-09-15_seesat_PP-Obs_three_paths.txt")
    iod_str = Path(input_filename).read_text().strip()

    # Run the method
    tdms = iod2tdm(iod_str, tdm_format='xml', originator='LSF')

    # Check the result
    assert (len(tdms) == 3)
    assert_string(tdms[0],
                  folder.joinpath('ccsds_tdm_2020-09-15_seesat_PP1.xml').read_text(),
                  ignore=IGNORE_STR)
