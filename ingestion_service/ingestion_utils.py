"""
This module provides the method for the ingestion service.
"""
import logging

import requests
from beyond.dates import Date
from beyond.io.ccsds import dumps

import settings
from forwarding_utils import forward_tdms
from imap_utils import IMAPConfig
from seesat_utils import fetch_seesat, mark_messages_as_seen
from tdmconvert import read_iod_lines

LOGGER = logging.getLogger(__name__)


class ForwardingError(Exception):
    """
    An error occured while trying to forward TDM messages.
    """


def iod_mails_to_tdm_xml_files(messages):
    """
    Convert a list of email messages with IOD lines to a list of CCSDS TDM files.

    Arguments:
    messages (list(dict)): List of email messages (complicated type).

    Returns:
    tdms (dict): Dictionary of CCSDS TDM files;
                 Keyword (str): Filename, e.g. LSF_2023-04-04T113242_64_4171_48648.xml
                 Value (str): CCSDS TDM file in XML format
    stats (dict): Dict with Counters for the total number of processed IOD measurements and
                  TDM messages.
    """
    stats = {'iods': 0, 'tdms': 0}

    tdms = {}
    tdm_counter = 0

    for message in messages:
        measures_collection = read_iod_lines(message['body'])
        for path_str, measures in measures_collection.items():
            satellite_id, station_id = path_str.split('-')
            creation_date = Date.now()
            tdm = dumps(measures,
                        fmt='xml',
                        creation_date=creation_date,
                        originator=settings.TDM_ORIGIN)
            tdms[f'{settings.TDM_ORIGIN}_'
                 f'{creation_date.datetime:%Y-%m-%dT%H%M%S}_'
                 f'{tdm_counter}_'
                 f'{station_id}_'
                 f'{satellite_id}'
                 '.xml'] = tdm
            tdm_counter += 1
            stats['iods'] += len(measures)

        stats['tdms'] += len(measures_collection)

    return tdms, stats


def get_tdms_from_seesat(logger=LOGGER):
    """
    This method performs the following tasks:
    - Fetch latest messages from the seesat-l mailinglist sent by
      any submitter in the allowlist, as received by the configured IMAP server.
    - Extract the IOD measurements
    - Convert those measurements to TDM messages
    - Output the number of IOD measurements and the number of created TDM messages
    """
    imap_config = IMAPConfig(settings.IMAP_USER, settings.IMAP_PASSWORD, settings.IMAP_HOSTNAME)

    messages = fetch_seesat(imap_config,
                            settings.SUBMITTER_ALLOWLIST,
                            filter_unseen=True,
                            peek=True)

    tdms, stats = iod_mails_to_tdm_xml_files(messages)

    logger.info(f"Fetched {len(messages)} messages with {stats['iods']} IOD measurements and "
                f"converted them to {stats['tdms']} TDM messages.")

    if len(tdms) >= 1:
        try:
            forward_tdms(settings.FORWARDING_SERVICE_BASE_URL, tdms)
        except requests.exceptions.RequestException as err:
            raise ForwardingError(
                f"Forwarding to {settings.FORWARDING_SERVICE_BASE_URL} failed with {err}.")

    # Only if the forwarding succeeded the messages are marked as 'read' on the server.
    msg_nums = [message['imap_msg_num'] for message in messages]
    mark_messages_as_seen(imap_config, msg_nums)

    return stats
