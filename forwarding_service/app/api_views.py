"""
This module provides the Forwarding Service API views with the following HTTP REST API endpoint:
- POST `/forward_tdms`
"""
# - Pylint doesn't dsicover app.webdav_utils (but it works when deployed)
# pylint: disable=import-error
import json
from http import HTTPStatus

import requests
from flask import Blueprint, Response, jsonify, request

import app.settings as settings

BP = Blueprint('api', __name__, url_prefix='')


@BP.route("/")
def hello_world():
    """
    This dummy endpoint returns just a dummy message and serves testing purposes only.
    """
    return jsonify(hello="world")


@BP.route("/forward_tdms", methods=["PUT"])
def forward_tdms():
    """
    This endpoint accepts a set of CCSDS TDM messages and forwards them via WebDAV.
    """
    data = json.loads(request.data)

    print(f"Received {len(data['tdms'])} TDM message(s), forwarding...")
    try:
        session = requests.Session()
        session.auth = (settings.WEBDAV_USERNAME, settings.WEBDAV_PASSWORD)

        for filename, data in data['tdms'].items():
            print(f"- {filename}")
            response = session.put(f"{settings.WEBDAV_URL}/{filename}", data=data)
            response.raise_for_status()

        print("Done.")
        return Response(json.dumps({'message': 'Success'}), mimetype='application/json')
    except requests.exceptions.RequestException as err:
        print(f"Upload failed with {err}")
        return Response(json.dumps({
            'message': 'Error',
            'details': str(err)
        }),
                        mimetype='application/json',
                        status=HTTPStatus.INTERNAL_SERVER_ERROR)
