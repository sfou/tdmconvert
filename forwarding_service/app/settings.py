"""
Settings module for the forwarding service
"""

from decouple import config

WEBDAV_URL = config('WEBDAV_URL', default='')
WEBDAV_USERNAME = config('WEBDAV_USERNAME', default='')
WEBDAV_PASSWORD = config('WEBDAV_PASSWORD', default='')
